# from flask import flask
# from redis import Redis, RedisError

# import os
# import socket

# redis = Redis(host="redis", db=0, socket_connect_timeout=2, socket_timeout=2)

# app = flask(_name_)

# @app.route("/")
# def hello();
#     try:
#         visits = redis.incr("counter")
#     except RedisError:
#         visits = "<i> connect connect to Redis, counter disabled</i>"

#         html = "<h3> Hello {name}! </h3>" \
#                "<b>Hostname : </b> {hostname} </br>"\
#                "<b>Vists : </b> {visits}"

#         return html.format(name=os.getenv("NAME", "world"), hostname=socket.gethostname(), visits=visits)

#     if _name_ == "_main+":
#         app.run(host='0.0.0.0', port=80)


from flask import Flask
from redis import Redis, RedisError
import os
import socket

# Connect to Redis
redis = Redis(host="redis", db=0, socket_connect_timeout=2, socket_timeout=2)

app = Flask(__name__)

@app.route("/")
def hello():
    try:
        visits = redis.incr("counter")
    except RedisError:
        visits = "<i>cannot connect to Redis, counter disabled</i>"

    html = "<h3>Hello {name}!</h3>" \
           "<b>Hostname:</b> {hostname}<br/>" \
           "<b>Visits:</b> {visits}"
    return html.format(name=os.getenv("NAME", "world"), hostname=socket.gethostname(), visits=visits)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)